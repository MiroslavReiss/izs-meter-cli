import json
import subprocess
import time
import os

#Enable or Disable DEBUG mode / True for 448.490Mhz False for IZS frequency
DEBUG = False
# Set parameters for debug mode
if DEBUG == True:
    FREQ_MIN = "448M"
    FREQ_MAX = "450.5M"
    STEP = "2.5M"
    #Set THRESHOLD for DEBUG mode
    THRESHOLD = -8
else:
    FREQ_MIN = "380M"
    FREQ_MAX = "382.5M"
    STEP = "2.5M"
    #Set THRESHOLD for IZS mode
    THRESHOLD = -8

# run rtl_power subprocess
process = subprocess.Popen(['rtl_power', '-f', f"{FREQ_MIN}:{FREQ_MAX}:{STEP}", '-g', '35', '-i', 's'], stdout=subprocess.PIPE)

while True:
    output = process.stdout.readline()
    if not output:
        break
    # parse values
    date, time, start_frequency, end_frequency, step_size, samples, min_power, max_power = output.strip().decode('utf-8').split(', ')

    if DEBUG == True:
        print(f'DEBUG mode: Date: {date}, Time: {time}, Start frequency: {start_frequency}, End frequency: {end_frequency}, Step size: {step_size}, Samples: {samples}, Min power: {min_power}, Max power: {max_power}')
    else:
        print(f'IZS mode: Date: {date}, Time: {time}, Start frequency: {start_frequency}, End frequency: {end_frequency}, Step size: {step_size}, Samples: {samples}, Min power: {min_power}, Max power: {max_power}')
    
    if float(max_power) > THRESHOLD:
        print("Peak detect! " + max_power + "dB")
        os.system(""" 
        raspi-gpio set 18 op
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #
        raspi-gpio set 18 dh
        sleep 0.042
        raspi-gpio set 18 dl
        sleep 0.057
        #
        #beeeeeep
        raspi-gpio set 18 dh
        sleep 1.478
        raspi-gpio set 18 dl
        sleep 1.493

        raspi-gpio set 18 dh
        sleep 1.478
        raspi-gpio set 18 dl
        sleep 1.493
        """)
